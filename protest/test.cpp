#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdint>
#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using namespace std;
#endif /* __PROGTEST__ */

long fib_pos( long n, long & tmp){
  if(n==0) {
      tmp = 1;
      return 0;
  }
  if (n==1) {
      tmp = 1;
      return 0;
  }
  int f[n];
  f[0]=1;
  f[1]=2;
  int i=2;
  for (i;f[i-1] <= n;i++){
      f[i]=f[i-1]+f[i-2];
  }
  tmp = f[i-2];
  return i-2;
}

int pow(int n, int a){
    if(a==0)
        return 1;
    int rez=n;
    while(a>1){
        rez = rez*n;
        a--;
    }
    return rez;
}
long from_b_to_d(string &s){
    long a,final=0;
    long tmp=stol(s);
    for(int t=0;tmp!=0;t++){
        a = tmp%10;
        tmp/=10;
        final += a * pow(2,t);
    }
    final ++;
    return final;

}
string fibb(string &s){
    long n = from_b_to_d(s);
    long tmp;
    long position=0;
    position= fib_pos(n, tmp);
    string f;
    if (n==1)
        return f="11";
    for(int i=0;i<=position;i++)
        f.append("0");
    cout << f <<' ' << n << endl;

    for(int t=0;n>0;t++){
        position= fib_pos(n, tmp);
        if(tmp<=n)
            f[position]='1';
        n=n-tmp;
        cout << "tmp="<< tmp << " n=" << n <<" pos=" << position <<  endl;

    }
    f.append("1");
    return f;

}
bool utf8ToFibonacci( const char * inFile, const char * outFile )
{   char a;
    string b;
    int set = 0;
    ifstream file;
    ofstream out;
    out.open(outFile);
    file.open(inFile, ios_base::binary | ios::in);
    if(!file.bad()){
        while(!file.eof()) {
            file.get(a);
            string tmp;
            b=bitset<8>((int) (unsigned char) a).to_string();
            if(b.rfind("110",0)==0){
                file.get(a);
                tmp=bitset<8>((int) (unsigned char) a).to_string();
                if(tmp.rfind("10",0)==0){
                    string r;
                    r.append(b,3,5);
                    r.append(tmp,2,6);

                }else
                    return false;
            }


            //cout << b<< "| " ;

        }
        //cout<<endl;

    }else
        return false;



  // todo
}
  
bool fibonacciToUtf8( const char * inFile, const char * outFile )
{
  // todo
}
  
#ifndef __PROGTEST__

bool identicalFiles( const char * file1,const char * file2 )
{
  // todo
}

int main ( int argc, char * argv [] )
{   string s="1011";


    cout << fibb(s) << endl;
    //utf8ToFibonacci( "example/src_2.utf8", "output.fib" );
  /*assert ( utf8ToFibonacci ( "example/src_0.utf8", "output.fib" )
           && identicalFiles ( "output.fib", "example/dst_0.fib" ) );
  assert ( utf8ToFibonacci ( "example/src_1.utf8", "output.fib" )
           && identicalFiles ( "output.fib", "example/dst_1.fib" ) );
  assert ( utf8ToFibonacci ( "example/src_2.utf8", "output.fib" )
           && identicalFiles ( "output.fib", "example/dst_2.fib" ) );
  assert ( utf8ToFibonacci ( "example/src_3.utf8", "output.fib" )
           && identicalFiles ( "output.fib", "example/dst_3.fib" ) );
  assert ( utf8ToFibonacci ( "example/src_4.utf8", "output.fib" )
           && identicalFiles ( "output.fib", "example/dst_4.fib" ) );
  assert ( ! utf8ToFibonacci ( "example/src_5.utf8", "output.fib" ) );
  assert ( fibonacciToUtf8 ( "example/src_6.fib", "output.utf8" )
           && identicalFiles ( "output.utf8", "example/dst_6.utf8" ) );
  assert ( fibonacciToUtf8 ( "example/src_7.fib", "output.utf8" )
           && identicalFiles ( "output.utf8", "example/dst_7.utf8" ) );
  assert ( fibonacciToUtf8 ( "example/src_8.fib", "output.utf8" )
           && identicalFiles ( "output.utf8", "example/dst_8.utf8" ) );
  assert ( fibonacciToUtf8 ( "example/src_9.fib", "output.utf8" )
           && identicalFiles ( "output.utf8", "example/dst_9.utf8" ) );
  assert ( fibonacciToUtf8 ( "example/src_10.fib", "output.utf8" )
           && identicalFiles ( "output.utf8", "example/dst_10.utf8" ) );
  assert ( ! fibonacciToUtf8 ( "example/src_11.fib", "output.utf8" ) );*/
 
  return EXIT_SUCCESS;
}
#endif /* __PROGTEST__ */
